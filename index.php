<!doctype html>
<html lang="en">
<head>
    <title>Angry Blacks</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="custom-css-bootstrap-magic-2017-12-29.css">
    <link rel="stylesheet" href="style.css">
</head>
<body id="xbg">

<?php require 'nav.php'; ?>
<br/>
<div class="container">
<div class="p-1 text-center">
        <a href="" ><img src="images/AngryBlacks_together.png" class="img-fluid" /></a>
        <a href="" ><img src="images/percy_black.png" class="img-fluid"  /></a>
</div>

<div class="row" id="bg">
<div class="col-md-12">
<center>
<a href="https://play.google.com/store/apps/details?id=com.angryblacksthegame.angryblacksstarringpercyblacks"><img src="images/Google_button2.png" title="Download Angry Blacks Free on Google Play" class="img-fluid" width="200px"></a>
</center>
<br/>
<p class="introText"><b>Cramberry Games, Inc</b> is very pleased to release the first socially conscious, sci-fi app game!
<br/>Join <i>Percy Blacks</i> and his Crew as they transform “The System” and restore balance to the people of Earth!!	  </p>
	<p class="introText">
		Just like in real life, this game is challenging in more ways than one. It challenges game playing skill, and<br>
also challenges thoughts about sensitive, and often uncomfortable, issues.  Cramberry believes we, as
human beings, must open our eyes, minds, and hearts, and dare to take a good look at the social, and<br>
economic realities we currently face.
	</p>

	<p class="introText" id="lastIntroText">
	Cramberry also, extends our belief of fairness and revenue equality by pledging
<br>to donate proceeds of our revenue to two, of many very worthy causes. We are excited
<br>to donate a portion of our proceeds to breast cancer research, and to children with autism.
<br>It’s fun to play games for entertainment, yet we believe gaming is more
<br>effective when it’s thought provoking, helps uplift, and unite people!
	</p>
	<p id="bottomIntroText">
	Thanks For Joining Us & Let The Cramberry Games Begin!!!
	</p>

	<div class="row">
			<div class="col-md-4" align="center"><a href="https://www.facebook.com/Angry-Blacks-The-Game-429535167230307/"><img src="images/Facebook.png"></a></div>
			<div class="col-md-4" align="center"><a href="https://twitter.com/angryblacksgame"><img src="images/Twitter.png"></a></div>
			<div class="col-md-4" align="center"><a href="https://instagram.com/angryblacks_thegame/"><img src="images/Instagram.png"></a></div>
	</div>
	<div class="youtube">
	 <iframe width="560" height="315" src="https://www.youtube.com/embed/mo8--zhJ9yE" frameborder="0" allowfullscreen></iframe>
	 </div>

<center>
	<p id="twitterAccountText">
		<a href="https://twitter.com/PercyBlacks">@PercyBlacks</a><br/>

		<a href="https://twitter.com/Phil_ABCrew">@Phil_ABCrew</a> <span class="twitterDot">•</span> <a href="https://twitter.com/Vanessa_ABCrew">@Vanessa_ABCrew</a> <span class="twitterDot">•</span> <a href="https://twitter.com/James_ABCrew">@James_ABCrew</a><br/>
<a href="https://twitter.com/Shannon_ABCrew">@Shannon_ABCrew</a> <span class="twitterDot">•</span> <a href="https://twitter.com/James_ABCrew">@James_ABCrew</a> <span class="twitterDot">•</span> <a href="https://twitter.com/Sky_ABCrew">@Sky_ABCrew</a> <span class="twitterDot">•</span> <a href="https://twitter.com/JayJay_ABCrew">@JayJay_ABCrew</a>
	</p>
</center>
</div>
<br/>

     <div align="center" class="text-center">

<!-- AddThis Button BEGIN -->
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=schnallity"><img src="http://s7.addthis.com/static/btn/sm-share-en.gif" width="83" height="16" alt="Bookmark and Share" style="border:0"/></a>
<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=schnallity"></script>
<!-- AddThis Button END -->
</div>



    <br/>
</div>

</div>

</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
        integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="./node_modules/validate.js/validate.js"></script>

</body>
</html>
