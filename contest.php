<?php
    require 'vendor/autoload.php';

    try {
        $dotenv = new Dotenv\Dotenv(__DIR__);
        $dotenv->load();
        $dotenv->required([
            'HIGH_SCORE_MAIL_FROM_SUBJECT',
            'MAIL_FROM_EMAIL',
            'MAIL_FROM_NAME',
            'MAIL_FROM_SUBJECT',
            'MAIL_TO_EMAIL',
            'SMTP_SERVER', 
            'SMTP_USERNAME', 
            'SMTP_PASSWORD', 
            'SMTP_PORT',
        ]);
    } catch (\Exception $e) {
        print "Invalid configuration.";
        exit;
    }
   
    if (!empty($_POST['was_submitted'])) {
        $first_name = $_POST['first_name']  ?: '';
        $email = $_POST['email'] ?: '';
        $board_level = $_POST['board_level'] ?: '';
        $high_score = $_POST['high_score'] ?: '';
        $city = $_POST['city'] ?: '';
        $state = $_POST['state'] ?: '';
        $country = $_POST['country'] ?: '';
        $success = false;
    
        if ($first_name && $email && $board_level && $high_score) {
            // display msg and send email 
            $msg = 'Thank you for submitting your high score!';

            // Create the Transport
            $transport = (new Swift_SmtpTransport(getenv('SMTP_SERVER'), getenv('SMTP_PORT')))
              ->setUsername(getenv('SMTP_USERNAME'))
              ->setPassword(getenv('SMTP_PASSWORD'))
            ;
            
            // Create the Mailer using your created Transport
            $mailer = new Swift_Mailer($transport);
            
            // Create a message
            $message = (new Swift_Message(getenv('HIGH_SCORE_MAIL_FROM_SUBJECT')))
              ->setFrom([getenv('MAIL_FROM_EMAIL') => getenv('MAIL_FROM_NAME')])
              ->setTo([getenv('MAIL_TO_EMAIL')])
              ->setBody(print_r($_POST, 1))
              ;
            
            // Send the message
            $result = $mailer->send($message);

            $success = true;
        } else {
            $msg = 'There were errors, please try again';
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <title>Angry Blacks</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="custom-css-bootstrap-magic-2017-12-29.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php require 'nav.php'; ?>
<br/>
<div class="container bg">


<div class="row">
<div class="col-md-12">
    <div class="text-center">
        <img src="./images/The.png" class="img-fluid" title="Angry Blacks" alt="Angry Blacks" width="10%"><br/>
        <img src="./images/AngryBlacks_together.png" class="img-fluid" title="Angry Blacks" alt="Angry Blacks" width="80%">
        <img src="./images/AppGameChallenge.png" class="img-fluid" title="Angry Blacks" alt="Angry Blacks" width="70%">
    </div>
    <div class="row">
        <div class="text-center">
            <img src="./images/1st500K.png" class="img-fluid" title="Angry Blacks" alt="Angry Blacks" width="90%">
        </div>
        <div class="text-center">
            <img src="./images/2nd250K.png" class="img-fluid" title="Angry Blacks" alt="Angry Blacks" width="90%">
        </div>
    </div>
    <div class="row text-center">
        <div class="text-center col-12">
            <img src="./images/3rd125K.png" class="img-fluid" title="Angry Blacks" alt="Angry Blacks" />
        </div>
    </div>
<div class="row" style="background-color:#ffffff; color: black; border-top: 7px solid red;">
    <div class="col-12">
        <h2 class="text-center">Take on the "Angry Blacks" App Game Challenge!</h2>
    <p class="text-center">
    The contest that allows players to compete for thousands of dollars in prize money!
    </p>
    <ol class="col-md-6 offset-md-3 myul">
        <li>SEARCH “Angry Blacks feat Percy Blacks” in Google play apps</li>
        <li>Play until you get your highest scores AND finish all 9 Levels</li>
        <li>Post all high scores for each Level and verify completing the game *</li>
    </ol>
    <p>
* - Players high scores will be added up along with verification of completing all 9 Levels. Finalists will be contacted via email to prove their winning information. Contest runs for 90 days.
    </p>
    </div>
</div>

<div class="row">
        <div class="offset-md-4">
        </div>
        <div class="col-md-3" style="padding-top: 60px;">
            <a href="https://play.google.com/store/apps/details?id=com.angryblacksthegame.angryblacksstarringpercyblacks" alt="Download Angry Blacks" title="Download Angry Blacks"><img src="./images/Google_button.png" class="img-fluid" /></a>
        </div>
</div>

<div class="row">
    <div class="col-12">
        <p>It’s simple! Download from Google, master the game, earn the highest scores possible for each Level AND complete all 9 Levels. Post your info and the top three players win huge cash prizes!</p>
    </div>
</div>

    <?php
        if (!empty($msg) && $success) {
    ?>
<div class="card">
  <div class="card-header">
    High Score Submitted!
  </div>
  <div class="card-body">
    <h5 class="card-title"><?php print $msg ?></h5>
    <p class="card-text">You will be contacted shortly</p>
    <a href="/" class="btn btn-primary">Back to homepage</a>
  </div>
</div>
<?php
        } else {
    ?>

    <form method="POST" id="register">
        <input type="hidden" name="was_submitted" value="1" />
        <div class="form-row">
            <div class="form-group col-md-4 offset-md-4">
                <label for="first_name">First Name</label>
                <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First Name">
                <div class="messages"></div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4 offset-md-4">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                <div class="messages"></div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4 offset-md-2">
                <label for="high_score">High Score</label>
                <input type="text" name="high_score" class="form-control" id="high_score" placeholder="High Score">
                <div class="messages"></div>
                <br/>
                <label for="city">City</label>
                <input type="text" name="city" class="form-control" id="city" placeholder="City">
                <div class="messages"></div>
                <br/>
                <label for="state">State (if applicable)</label>
                <input type="text" name="state" class="form-control" id="state" placeholder="State">
                <div class="messages"></div>
                <br/>
                <label for="country">Country</label>
                <input type="text" name="country" class="form-control" id="country" placeholder="Country">
                <div class="messages"></div>


            </div>
            <div class="form-group col-md-4">
                <label for="board_level">Board/Level</label>
                <select class="form-control" name="board_level" id="size">
                    <option value="">-- select board / level</option>
                    <option value="1">Saggin' Mall - Level 1</option>
                    <option value="2">Saggin' Mall - Level 2</option>
                    <option value="3">Saggin' Mall - Level 3</option>
                    <option value="4">Corrupt Cop - Level 1</option>
                    <option value="5">Corrupt Cop - Level 2</option>
                    <option value="6">Corrupt Cop - Level 3</option>
                    <option value="7">Murder Burger- Level 1</option>
                    <option value="8">Murder Burger- Level 2</option>
                    <option value="9">Murder Burger- Level 3</option>
                </select>
                <div class="messages"></div>
            </div>
        </div>

        <div class="form-row">
            <div class="col col-sm-1">&nbsp;</div>
        </div>
        <div class="form-group col-md-4 offset-md-5">
            <input type="submit" class="btn btn-primary" value="Submit" />
        </div>
    </form>
<?php
    }
?>

<!--
    <br/>
    <p class="text-center">
        <img src="/images/AB_originalsoundtrack.png" class="img-fluid" title="Angry Blacks - Original Soundtrack" alt="Angry Blacks - Original Soundtrack"/>
    </p>
    <p class="text-center">
        If you haven't heard the Angry Blacks soundtrack, it's available on iTunes and Google Play.  Listen to samples ...
    </p>

    <div class="row justify-content-md-center">
        <div>
            <a href="https://itunes.apple.com/us/album/angry-blacks-original-game-soundtrack/1252010239" alt="Download on iTunes" title="Download on iTunes">
            <img src="./images/iTunes_button.png" width="120px"/>
            </a>
        </div>
        <div>
            <a href="https://play.google.com/store/music/album/Various_Artists_Angry_Blacks_Original_Game_Soundtr?id=Betnknucc4yygbrzxyapgvwzlmu&hl=en" alt="Get it on Google Play" title="Get it on Google Play">
            <img src="./images/Google_button.png" width="120px"/>
            </a>
        </div>
    </div>
    <br/>

    <h3 class="text-center" style="color: #FCBE2C; align: center;">PLAY THE GAME TO WIN THE GAME!</H3>
</div>
-->

</div>

</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
        integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="./node_modules/validate.js/validate.js"></script>

    <script>
(function() {
      // Hook up the form so we can prevent it from being posted
      var form = document.querySelector("form#register");
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        if (handleFormSubmit(form)) {
            form.submit();
        }
      });

        var constraints = {
            first_name: {
                presence: true,
            },
            email: {
                presence: true,
                email: true
            },
            board_level: {
                presence: true,
            }
        }

      // Hook up the inputs to validate on the fly
      // var inputs = document.querySelectorAll("input, textarea, select")
      // for (var i = 0; i < inputs.length; ++i) {
      //   inputs.item(i).addEventListener("change", function(ev) {
      //     var errors = validate(form, constraints) || {};
      //     showErrorsForInput(this, errors[this.name])
      //   });
      // }

      function handleFormSubmit(form, input) {
        // validate the form against the constraints
        var errors = validate(form, constraints);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
            return true;
          // showSuccess();
        }
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "form-group");

        if (formGroup) {
            // Find where the error messages will be insert into
            var messages = formGroup.querySelector(".messages");
            // First we remove any old messages and resets the classes
            resetFormGroup(formGroup);
            // If we have errors
            if (errors) {
              // we first mark the group has having errors
              formGroup.classList.add("has-error");
              // then we append all the errors
              _.each(errors, function(error) {
                addError(messages, error);
              });
            } else {
              // otherwise we simply mark it as success
              formGroup.classList.add("has-success");
            }
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it \:D/
        alert("Success!");
      }
})();
    </script>
</body>
</html>
