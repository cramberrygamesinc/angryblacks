module.exports = {
  entry: {
    vendor: [
        'validate.js'
    ]
  },
  output: {
    filename: 'bundle.js'
  },
  resolve: {
    modules: [
        'node_modules',
    ]
  },
  module: {
    rules: [
        {
           test   : /validate\.min\.js/,
            loader : 'script' 
        }
    ]
  }
};
