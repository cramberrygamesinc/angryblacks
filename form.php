<?php
    require 'vendor/autoload.php';

    try {
        $dotenv = new Dotenv\Dotenv(__DIR__);
        $dotenv->load();
        $dotenv->required([
            'HIGH_SCORE_MAIL_FROM_SUBJECT',
            'MAIL_FROM_EMAIL',
            'MAIL_FROM_NAME',
            'MAIL_FROM_SUBJECT',
            'MAIL_TO_EMAIL',
            'SMTP_SERVER', 
            'SMTP_USERNAME', 
            'SMTP_PASSWORD', 
            'SMTP_PORT',
        ]);
    } catch (\Exception $e) {
        print "Invalid configuration.";
        exit;
    }
   
    if (!empty($_POST['was_submitted'])) {
        $first_name = $_POST['first_name']  ?: '';
        $email = $_POST['email'] ?: '';
        $size = $_POST['size'] ?: '';
        $shirt = $_POST['shirt'] ?: '';
        $success = false;
    
        if ($first_name && $email && $size && $shirt) {
            // display msg and send email 
            $msg = 'Thank you for registering!';

            // Create the Transport
            $transport = (new Swift_SmtpTransport(getenv('SMTP_SERVER'), getenv('SMTP_PORT')))
              ->setUsername(getenv('SMTP_USERNAME'))
              ->setPassword(getenv('SMTP_PASSWORD'))
            ;
            
            // Create the Mailer using your created Transport
            $mailer = new Swift_Mailer($transport);
            
            // Create a message
            $message = (new Swift_Message(getenv('MAIL_FROM_SUBJECT')))
              ->setFrom([getenv('MAIL_FROM_EMAIL') => getenv('MAIL_FROM_NAME')])
              ->setTo([getenv('MAIL_TO_EMAIL')])
              ->setBody(print_r($_POST, 1))
              ;
            
            // Send the message
            $result = $mailer->send($message);

            $success = true;
        } else {
            $msg = 'There were errors, please try again';
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <title>Angry Blacks</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="custom-css-bootstrap-magic-2017-12-29.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php require 'nav.php'; ?>
<br/>
<div class="container">


<div class="row">
<div class="col-md-3 shirtcol">
<img src="./images/WePlayed_FRONT.png" class="img-fluid shirt">
<br/>
<br/>
    <img src="./images/WePlayed_BACK.png" class="img-fluid shirt">
</div>
<div class="col-md-6">
    <img src="./images/angryblacks.png" class="img-fluid" title="Angry Blacks" alt="Angry Blacks">
    <h3 class="text-center" style="color: #FCBE2C;">WE'RE EXCITED TO ANNOUNCE T-SHIRTS WILL SOON BE AVAILABLE</h2>
    <p>
        Most people throughout the world are angry about things happening on the planet. Cramberry Games would like to
        shift anger into energy that can achieve goals.
    </p>

    <?php
        if (!empty($msg) && $success) {
    ?>
<div class="card">
  <div class="card-header">
    Registration completed
  </div>
  <div class="card-body">
    <h5 class="card-title"><?php print $msg ?></h5>
    <p class="card-text">You will be contacted shortly</p>
    <a href="/" class="btn btn-primary">Back to homepage</a>
  </div>
</div>
<?php
        } else {
    ?>

    <p>
        Fill in the fields below to be eligible for t-shirts in the near future!
    </p>

    <form method="POST" id="register">
        <input type="hidden" name="was_submitted" value="1" />
        <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First Name">
            <div class="messages"></div>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Email">
            <div class="messages"></div>
        </div>

        <div class="form-row">

            <div class="col">
                <div class="form-group">
                    <label for="size">Size</label>
                    <select class="form-control" name="size" id="size">
                        <option value="">-- select size</option>
                        <option value="S">-S-</option>
                        <option value="M">-M-</option>
                        <option value="L">-L-</option>
                        <option value="XL">-XL-</option>
                        <option value="XXL">-XXL-</option>
                    </select>
                    <div class="messages"></div>
                </div>
            </div>

            <div class="col col-sm-1">&nbsp;</div>
            <div class="col">
                <br/>
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="shirt[]" value="We Played the Game">
                            "We Played the Game"
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="shirt[]" value="Change Anger to Aspiration">
                            "Change Anger to Aspiration"
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" class="btn btn-primary" value="Submit" />
    </form>
<?php
    }
?>

    <br/>
    <p class="text-center">
        <img src="/images/AB_originalsoundtrack.png" class="img-fluid" title="Angry Blacks - Original Soundtrack" alt="Angry Blacks - Original Soundtrack"/>
    </p>
    <p>
        If you haven't heard the Angry Blacks soundtrack, it's available on iTunes and Google Play.  Listen to samples ...
    </p>

    <div class="row justify-content-md-center">
        <div>
            <a href="https://itunes.apple.com/us/album/angry-blacks-original-game-soundtrack/1252010239" alt="Download on iTunes" title="Download on iTunes">
            <img src="./images/iTunes_button.png" width="120px"/>
            </a>
        </div>
        <div>
            <a href="https://play.google.com/store/music/album/Various_Artists_Angry_Blacks_Original_Game_Soundtr?id=Betnknucc4yygbrzxyapgvwzlmu&hl=en" alt="Get it on Google Play" title="Get it on Google Play">
            <img src="./images/Google_button.png" width="120px"/>
            </a>
        </div>
    </div>
    <br/>

    <h3 class="text-center" style="color: #FCBE2C; align: center;">PLAY THE GAME TO WIN THE GAME!</H3>
</div>
<div class="col-md-3 shirtcol">
    <img src="./images/Aspiration_FRONT.png" class="img-fluid shirt">
<br/>
<br/>
    <img src="./images/Aspiration_BACK.png" class="img-fluid shirt">
</div>


</div>

</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
        integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="./node_modules/validate.js/validate.js"></script>

    <script>
(function() {
      // Hook up the form so we can prevent it from being posted
      var form = document.querySelector("form#register");
      form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        if (handleFormSubmit(form)) {
            form.submit();
        }
      });

        var constraints = {
            first_name: {
                presence: true,
            },
            email: {
                presence: true,
                email: true
            },
            size: {
                presence: true,
            }
        }

      // Hook up the inputs to validate on the fly
      // var inputs = document.querySelectorAll("input, textarea, select")
      // for (var i = 0; i < inputs.length; ++i) {
      //   inputs.item(i).addEventListener("change", function(ev) {
      //     var errors = validate(form, constraints) || {};
      //     showErrorsForInput(this, errors[this.name])
      //   });
      // }

      function handleFormSubmit(form, input) {
        // validate the form against the constraints
        var errors = validate(form, constraints);
        // then we update the form to reflect the results
        showErrors(form, errors || {});
        if (!errors) {
            return true;
          // showSuccess();
        }
      }

      // Updates the inputs with the validation errors
      function showErrors(form, errors) {
        // We loop through all the inputs and show the errors for that input
        _.each(form.querySelectorAll("input[name], select[name]"), function(input) {
          // Since the errors can be null if no errors were found we need to handle
          // that
          showErrorsForInput(input, errors && errors[input.name]);
        });
      }

      // Shows the errors for a specific input
      function showErrorsForInput(input, errors) {
        // This is the root of the input
        var formGroup = closestParent(input.parentNode, "form-group");

        if (formGroup) {
            // Find where the error messages will be insert into
            var messages = formGroup.querySelector(".messages");
            // First we remove any old messages and resets the classes
            resetFormGroup(formGroup);
            // If we have errors
            if (errors) {
              // we first mark the group has having errors
              formGroup.classList.add("has-error");
              // then we append all the errors
              _.each(errors, function(error) {
                addError(messages, error);
              });
            } else {
              // otherwise we simply mark it as success
              formGroup.classList.add("has-success");
            }
        }
      }

      // Recusively finds the closest parent that has the specified class
      function closestParent(child, className) {
        if (!child || child == document) {
          return null;
        }
        if (child.classList.contains(className)) {
          return child;
        } else {
          return closestParent(child.parentNode, className);
        }
      }

      function resetFormGroup(formGroup) {
        // Remove the success and error classes
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        // and remove any old messages
        _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
          el.parentNode.removeChild(el);
        });
      }

      // Adds the specified error with the following markup
      // <p class="help-block error">[message]</p>
      function addError(messages, error) {
        var block = document.createElement("p");
        block.classList.add("help-block");
        block.classList.add("error");
        block.innerText = error;
        messages.appendChild(block);
      }

      function showSuccess() {
        // We made it \:D/
        alert("Success!");
      }
})();
    </script>
</body>
</html>
